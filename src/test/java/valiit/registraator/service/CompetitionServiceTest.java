package valiit.registraator.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import valiit.registraator.model.Competition;
import valiit.registraator.repository.CompetitionRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CompetitionServiceTest {

    @Mock
    private CompetitionRepository repository;

    @InjectMocks
    private CompetitionService service;

    @Test
    public void shouldAddCompetition() {
        List<Competition> competitionList = new ArrayList<>();
        competitionList.add(getCompetition("second_name"));
        when(repository.findAll()).thenReturn(competitionList);

        Competition newCompetition = getCompetition("first_name");
        service.addCompetition(newCompetition);

        verify(repository).save(newCompetition);
    }

    @Test
    public void shouldNotAddCompetition() {
        List<Competition> competitionList = new ArrayList<>();
        competitionList.add(getCompetition("second_name"));
        when(repository.findAll()).thenReturn(competitionList);

        Competition newCompetition = getCompetition("second_name");
        service.addCompetition(newCompetition);

        verify(repository, never()).save(newCompetition);
    }

    private Competition getCompetition(String name) {
        LocalDate date = LocalDate.now();
        Competition competition = new Competition();
        competition.setName(name);
        competition.setDate(date);
        return competition;
    }

}
