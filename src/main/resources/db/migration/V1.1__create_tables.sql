CREATE TABLE public.competitions
(
    id BIGSERIAL PRIMARY KEY,
    date date NOT NULL,
    description text NOT NULL,
    name VARCHAR(200) NOT NULL,
    place VARCHAR(200) NOT NULL,
    "time" time without time zone NOT NULL
);

CREATE TABLE public.participants
(
    id BIGSERIAL PRIMARY KEY,
    birth_year integer NOT NULL,
    club VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    name VARCHAR(50) NOT NULL,
    sex VARCHAR(1) NOT NULL,
    surename VARCHAR(50) NOT NULL,
    id_competition bigint NOT NULL,
    CONSTRAINT fkeky8lnlh44i9cafslr9j60ce5 FOREIGN KEY (id_competition)
            REFERENCES public.competitions (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE public.results
(
    id_competition bigint NOT NULL,
    id_participant bigint NOT NULL,
    finish bigint NOT NULL,
    "number" integer NOT NULL,
    start bigint NOT NULL,
    CONSTRAINT results_pkey PRIMARY KEY (id_competition, id_participant)
);

CREATE TABLE public.users
(
    id BIGSERIAL PRIMARY KEY,
    password VARCHAR(200) NOT NULL,
    username VARCHAR(200) NOT NULL
);
