package valiit.registraator.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Embeddable
public class ResultIdentity implements Serializable{

    @NotNull
    private Long idParticipant;
    @NotNull
    private Long idCompetition;
}
