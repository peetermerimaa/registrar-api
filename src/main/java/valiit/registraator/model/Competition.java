package valiit.registraator.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

@Data
@ToString(exclude = "participants")
@Table(name = "competitions")
@Entity
public class Competition{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 200)
    @ApiModelProperty(example = "Comp")
    private String name;

    @Column(nullable = false, length = 200)
    @ApiModelProperty(example = "Somwhere")
    private String place;

    @Column(nullable = false)
    @ApiModelProperty(example = "2019-10-10")
    private LocalDate date;

    @Column(nullable = false)
    @ApiModelProperty(example = "10:00:00")
    private LocalTime time;

    @Column(nullable = false)
    @Type(type = "org.hibernate.type.TextType")
    @ApiModelProperty(example = "text text text")
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "competition")
    @JsonIgnore
    private Set<Participant> participants;

}
