package valiit.registraator.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "participants")
@Entity
public class Participant{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    @ApiModelProperty(example = "Nimi")
    private String name;

    @Column(nullable = false, length = 50)
    @ApiModelProperty(example = "Perenimi")
    private String surename;

    @Column(nullable = false)
    @ApiModelProperty(example = "1980")
    private int birthYear;

    @Column(nullable = false, length = 1)
    @ApiModelProperty(example = "M")
    private String sex;

    @Column(nullable = false, length = 50)
    @ApiModelProperty(example = "Klubi")
    private String club;

    @Column(nullable = false, length = 50)
    @ApiModelProperty(example = "high@tech.com")
    private String email;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnore
    @JoinColumn(name = "id_competition", referencedColumnName = "id")
    private Competition competition;

}
