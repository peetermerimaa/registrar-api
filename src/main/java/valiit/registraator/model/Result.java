package valiit.registraator.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name = "results")
@Entity
public class Result{

    @Column(nullable = false)
    private int number;

    @Column(nullable = false)
    private Long start;

    @Column(nullable = false)
    private Long finish;

    @EmbeddedId
    private ResultIdentity resultIdentity;

}
