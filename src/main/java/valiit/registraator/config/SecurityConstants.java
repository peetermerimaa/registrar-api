package valiit.registraator.config;

public class SecurityConstants{
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/api/v1/sign-up";
    public static final String PARTICIPANT_CONTROLLER = "/api/v1/participant/**";
    public static final String GET_COMPETITIONS = "/api/v1/competition/get/**";
    public static final String TOKEN_EXAMPLE = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiJ9.JUGDZs3Eyf6G08Y9JsGHxLBscBWsiJVeKfsH1302nCYO1xiwASrsV_I5TcAL8Dc06K0eOTXncVFmWHCyWhw1fA";
}
