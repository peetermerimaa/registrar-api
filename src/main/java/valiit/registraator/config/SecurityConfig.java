package valiit.registraator.config;

import valiit.registraator.User.UserService;
import valiit.registraator.auth.JwtAuthenticationFilter;
import valiit.registraator.auth.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

import static valiit.registraator.config.SecurityConstants.*;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    protected void configure( AuthenticationManagerBuilder auth ) throws Exception{
        auth.userDetailsService( userService ).passwordEncoder( passwordEncoder );
    }

    @Override
    public void configure( WebSecurity web ){
        web.ignoring().antMatchers( "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**" );
    }

    @Override
    protected void configure( HttpSecurity http ) throws Exception{
        http.cors().and().csrf().disable().authorizeRequests()
            .antMatchers( HttpMethod.POST, SIGN_UP_URL ).permitAll()
            .antMatchers( PARTICIPANT_CONTROLLER ).permitAll()
            .antMatchers( HttpMethod.GET, GET_COMPETITIONS ).permitAll()
            .anyRequest().authenticated().and()
            .addFilter( new JwtAuthenticationFilter( authenticationManager(), jwtSecret ) )
            .addFilter( new JwtAuthorizationFilter( authenticationManager(), jwtSecret ) )
            .sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS );
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource(){
        CorsConfiguration configuration = new CorsConfiguration().applyPermitDefaultValues();
        configuration.setAllowedOrigins( Arrays.asList( "*" ) );
        configuration.setAllowedMethods( Arrays.asList( "POST", "GET", "PUT", "OPTIONS", "DELETE" ) );
        configuration.setAllowedHeaders( Arrays.asList( "*" ) );
        configuration.setExposedHeaders( Arrays.asList( "Authorization", "Content-Type" ) );

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration( "/**", configuration );
        return source;
    }
}
