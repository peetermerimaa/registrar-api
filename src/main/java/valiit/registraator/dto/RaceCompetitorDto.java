package valiit.registraator.dto;

import lombok.Data;

@Data
public class RaceCompetitorDto{

    private int number;
    private String name;
    private String surename;
    private int birthYear;
    private String sex;
    private String club;

}
