package valiit.registraator.dto;

import lombok.Data;

@Data
public class SubmitCompetitorDto{

    private Long id_competition;
    private Long id_participant;
    private int number;
}
