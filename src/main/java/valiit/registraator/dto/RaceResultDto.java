package valiit.registraator.dto;

import lombok.Data;

@Data
public class RaceResultDto{

    private int number;
    private String name;
    private String surename;
    private int birthYear;
    private String sex;
    private String club;
    private String start;
    private String finish;
    private String raceTime;

}
