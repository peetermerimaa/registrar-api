package valiit.registraator.dto;

import lombok.Data;

@Data
public class SubmitTimeDto{
    private Long id_competition;
    private int number;
    private Long stopwatch;

}
