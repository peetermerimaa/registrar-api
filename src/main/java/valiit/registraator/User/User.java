package valiit.registraator.User;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "users")
public class User{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 200)
    private String username;
    @Column(nullable = false, length = 200)
    private String password;
}
