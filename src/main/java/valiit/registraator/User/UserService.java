package valiit.registraator.User;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserService implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;

    public void addUser( User user ){
        if( isNew( user ) ){
            userRepository.save( user );
        }
    }

    public User getUser( String username ){
        return userRepository.findByUsername( username );
    }

    private boolean isNew( User user ){
        List<User> userList = userRepository.findAll();
        for( User databaseUser : userList ){
            if( databaseUser.getUsername().equals( user.getUsername() ) ){
                return false;
            }
        }
        return true;
    }

    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException{
        User user = getUser( username );
        return new org.springframework.security.core.userdetails.User(
            user.getUsername(), user.getPassword(), Collections.emptyList() );
    }

}
