package valiit.registraator.controller;

import valiit.registraator.dto.RaceCompetitorDto;
import valiit.registraator.dto.RaceResultDto;
import valiit.registraator.model.Participant;
import valiit.registraator.service.ParticipantService;
import valiit.registraator.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/participant")
public class ParticipantController{

    @Autowired
    ParticipantService participantService;

    @Autowired
    ResultService resultService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public List<Participant> getParticipantList(){
        return participantService.getParticipantList();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{idCompetition}")
    public Long addParticipant( @RequestBody Participant participant,
                                @PathVariable Long idCompetition ){
        return participantService.addParticipant( participant, idCompetition );
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteParticipant( @PathVariable Long id ){
        participantService.deleteParticipant( id );
    }

    @RequestMapping(method = RequestMethod.GET, value = "/competitors/{idCompetition}")
    public List<RaceCompetitorDto> getCompetitorList( @PathVariable Long idCompetition ){
        return resultService.getCompetitorList( idCompetition );
    }

    @RequestMapping(method = RequestMethod.GET, value = "/results/{idCompetition}")
    public List<RaceResultDto> getResultList( @PathVariable Long idCompetition ){
        return resultService.getRaseResultList( idCompetition );
    }
}
