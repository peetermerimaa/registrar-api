package valiit.registraator.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.lang.annotation.*;

import static valiit.registraator.config.SecurityConstants.TOKEN_EXAMPLE;

@Inherited
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@ApiImplicitParams({
    @ApiImplicitParam(
        name = "Authorization",
        value = "Authorization token",
        required = true,
        dataType = "string",
        paramType = "header",
        defaultValue = TOKEN_EXAMPLE)}) @interface AddSwaggerToken{
}
