package valiit.registraator.controller;

import org.springframework.web.bind.annotation.*;
import valiit.registraator.model.Competition;
import valiit.registraator.service.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RestController
@RequestMapping("/api/v1/competition")
public class CompetitionController{

    @Autowired
    CompetitionService competitionService;

    @RequestMapping(method = RequestMethod.GET, value = "/get")
    public List<Competition> getCompetitionList(){
        return competitionService.getCompetitionList();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Competition getCompetition( @PathVariable Long id ){
        return competitionService.getCompetition( id );
    }

    @RequestMapping(method = RequestMethod.POST, value = "/")
    @AddSwaggerToken
    public void addCompetition( @RequestBody Competition competition ){
        competitionService.addCompetition( competition );
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    @AddSwaggerToken
    public void deleteCompetition( @PathVariable Long id ){
        competitionService.deleteCompetition( id );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/")
    @AddSwaggerToken
    public void editCompetition( @RequestBody Competition competition ){
        competitionService.editCompetition( competition );
    }

}
