package valiit.registraator.controller;

import org.springframework.web.bind.annotation.*;
import valiit.registraator.dto.SubmitCompetitorDto;
import valiit.registraator.dto.SubmitTimeDto;
import valiit.registraator.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/api/v1/result")
public class ResultController{

    @Autowired
    ResultService resultService;

    @RequestMapping(method = RequestMethod.POST, value = "/")
    @AddSwaggerToken
    public boolean addCompetitor( @PathVariable SubmitCompetitorDto competitorDto ){
        return resultService.addCompetitor( competitorDto );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/time")
    @AddSwaggerToken
    public void editResultTime( @RequestBody SubmitTimeDto submitTimeDto ){
        resultService.editResultTime( submitTimeDto );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/number")
    @AddSwaggerToken
    public void setNumber( @RequestBody SubmitCompetitorDto submitCompetitorDto ){
        resultService.setNumber( submitCompetitorDto );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/finish")
    @AddSwaggerToken
    public void addStopwatchFinish( @RequestBody SubmitTimeDto[] submitTimeDtoList ){
        resultService.addStopwatchFinish( submitTimeDtoList );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/start")
    @AddSwaggerToken
    public void addStopwatchStart( @RequestBody SubmitTimeDto submitTimeDto ){
        resultService.addStopwatchStart( submitTimeDto );
    }
}
