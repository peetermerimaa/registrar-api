package valiit.registraator.service;

import valiit.registraator.dto.RaceCompetitorDto;
import valiit.registraator.dto.RaceResultDto;
import valiit.registraator.dto.SubmitCompetitorDto;
import valiit.registraator.dto.SubmitTimeDto;
import valiit.registraator.model.Participant;
import valiit.registraator.model.Result;
import valiit.registraator.model.ResultIdentity;
import valiit.registraator.repository.ResultRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ResultService{

    @Autowired
    private ResultRepository resultRepository;
    @Autowired
    private ParticipantService participantService;
    @Autowired
    private CompetitionService competitionService;

    public void addStopwatchFinish( SubmitTimeDto[] submitTimeDtoList ){
        for( SubmitTimeDto submitTimeDto : submitTimeDtoList ){
            List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( submitTimeDto.getId_competition() );
            for( Result result : resultList ){
                if( result.getNumber() == submitTimeDto.getNumber() ){
                    result.setFinish( submitTimeDto.getStopwatch() );
                    resultRepository.save( result );
                }
            }
        }
    }

    public void addStopwatchStart( SubmitTimeDto submitTimeDto ){
        List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( submitTimeDto.getId_competition() );
        for( Result result : resultList ){
            result.setStart( submitTimeDto.getStopwatch() );
            resultRepository.save( result );
        }
    }

    public void editResultTime( SubmitTimeDto submitTimeDto ){
        List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( submitTimeDto.getId_competition() );
        for( Result result : resultList ){
            if( submitTimeDto.getNumber() == result.getNumber() ){
                result.setFinish( submitTimeDto.getStopwatch() );
                resultRepository.save( result );
            }
        }
    }

    public void setNumber( SubmitCompetitorDto submitCompetitorDto ){
        List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( submitCompetitorDto.getId_competition() );
        for( Result result : resultList ){
            if( submitCompetitorDto.getId_participant() == result.getResultIdentity().getIdParticipant() ){
                result.setNumber( submitCompetitorDto.getNumber() );
                resultRepository.save( result );
            }
        }
    }

    public List<RaceCompetitorDto> getCompetitorList( Long idCompetition ){
        List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( idCompetition );
        List<RaceCompetitorDto> raceCompetitorDtoList = new ArrayList<>();
        for( Result result : resultList ){
            RaceCompetitorDto raceCompetitorDto = new RaceCompetitorDto();
            raceCompetitorDto.setNumber( result.getNumber() );
            Participant participant = participantService.getParticipant( result.getResultIdentity().getIdParticipant() );
            raceCompetitorDto.setName( participant.getName() );
            raceCompetitorDto.setSurename( participant.getSurename() );
            raceCompetitorDto.setBirthYear( participant.getBirthYear() );
            raceCompetitorDto.setClub( participant.getClub() );
            raceCompetitorDto.setSex( participant.getSex() );
            raceCompetitorDtoList.add( raceCompetitorDto );
        }
        return raceCompetitorDtoList;
    }

    public List<RaceResultDto> getRaseResultList( Long idCompetition ){
        List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( idCompetition );
        List<RaceResultDto> raceResultDtoList = new ArrayList<>();
        for( Result result : resultList ){
            RaceResultDto raceResultDto = new RaceResultDto();
            raceResultDto.setNumber( result.getNumber() );
            Date dt = new Date();
            SimpleDateFormat f = new SimpleDateFormat( "HH:mm:ss:S" );
            dt.setTime( result.getStart() );
            raceResultDto.setStart( f.format( dt ) );
            dt.setTime( result.getFinish() );
            raceResultDto.setFinish( f.format( dt ) );
            raceResultDto.setRaceTime( makeString( result.getFinish() - result.getStart() ) );
            Participant participant = participantService.getParticipant( result.getResultIdentity().getIdParticipant() );
            raceResultDto.setName( participant.getName() );
            raceResultDto.setSurename( participant.getSurename() );
            raceResultDto.setBirthYear( participant.getBirthYear() );
            raceResultDto.setSex( participant.getSex() );
            raceResultDto.setClub( participant.getClub() );
            raceResultDtoList.add( raceResultDto );
        }
        log.info( "raceResultDtoList is full {}", raceResultDtoList );
        sortListByWinner( raceResultDtoList );
        log.info( "raceResultDtoList is sorted out {}", raceResultDtoList );
        return raceResultDtoList;
    }

    public List<Result> getResultList( Long idCompetition ){
        List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( idCompetition );
        return resultList;
    }

    public boolean addCompetitor( SubmitCompetitorDto competitor ){
        List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( competitor.getId_competition() );
        if( resultList != null ){
            for( Result result : resultList ){
                if( competitor.getId_participant() == result.getResultIdentity().getIdParticipant() ){
                    return false;
                }
            }
        }
        Result newCompetitor = new Result();
        ResultIdentity identity = new ResultIdentity();
        identity.setIdCompetition( competitor.getId_competition() );
        identity.setIdParticipant( competitor.getId_participant() );
        newCompetitor.setResultIdentity( identity );
        newCompetitor.setStart( 0L );
        newCompetitor.setFinish( 0L );
        resultRepository.save( newCompetitor );
        return true;
    }

    public void deleteResultsByIdParticipant( Long idParticipant ){
        List<Result> resultList = resultRepository.findByResultIdentityIdParticipant( idParticipant );
        for( Result result : resultList ){
            resultRepository.delete( result );
        }
    }

    public void deleteResultsByIdCompetition( Long idCompetition ){
        List<Result> resultList = resultRepository.findByResultIdentityIdCompetition( idCompetition );
        for( Result result : resultList ){
            Long idParticipant = result.getResultIdentity().getIdParticipant();
            resultRepository.delete( result );
            participantService.deleteParticipant( idParticipant );
        }
    }

    private String makeString( Long raceTime ){
        return String.format( "%02d:%02d:%02d:%02d",
            TimeUnit.MILLISECONDS.toMinutes( raceTime ) / 60,
            TimeUnit.MILLISECONDS.toMinutes( raceTime ) % 60,
            TimeUnit.MILLISECONDS.toSeconds( raceTime ) -
                TimeUnit.MINUTES.toSeconds( TimeUnit.MILLISECONDS.toMinutes( raceTime ) ),
            raceTime - TimeUnit.MILLISECONDS.toSeconds( raceTime ) * 1000
        );
    }

    private void sortListByWinner( List<RaceResultDto> list ){
        boolean sorted = false;
        while( !sorted ){
            sorted = true;
            for( int i = 0; i < (list.size() - 1); i++ ){
                String a = list.get( i ).getRaceTime();
                String b = list.get( i + 1 ).getRaceTime();
                log.info( "b.compareTo(a) {}", b.compareTo( a ) );
                if( b.compareTo( a ) < 0 ){
                    RaceResultDto temp = list.get( i );
                    list.set( i, list.get( i + 1 ) );
                    list.set( (i + 1), temp );
                    sorted = false;
                }
                log.info( "{} is done, sorted = {}", i, sorted );
            }
        }
    }

}
