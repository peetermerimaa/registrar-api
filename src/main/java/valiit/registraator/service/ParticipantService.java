package valiit.registraator.service;

import valiit.registraator.dto.SubmitCompetitorDto;
import valiit.registraator.model.Competition;

import valiit.registraator.model.Participant;
import valiit.registraator.model.Result;
import valiit.registraator.repository.ParticipantRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ParticipantService{

    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private CompetitionService competitionService;
    @Autowired
    private ResultService resultService;

    public List<Participant> getParticipantList(){
        return participantRepository.findAll();
    }

    public Participant getParticipant( Long id ){
        return participantRepository.findById( id ).orElse( null );
    }

    public Long addParticipant( Participant participant, Long idCompetition ){
        if( isNew( participant, idCompetition ) ){
            Competition competition = competitionService.getCompetition( idCompetition );
            participant.setCompetition( competition );
            participantRepository.save( participant );
            Long idParticipant = getID( participant );
            SubmitCompetitorDto competitor = new SubmitCompetitorDto();
            competitor.setId_participant( idParticipant );
            competitor.setId_competition( idCompetition );
            resultService.addCompetitor( competitor );
            return idParticipant;
        }
        return -1L;
    }

    public void deleteParticipant( Long id ){
        if( participantRepository.existsById( id ) ){
            resultService.deleteResultsByIdParticipant( id );
            participantRepository.deleteById( id );
        }
    }

    private boolean isNew( Participant participant, Long idCompetition ){
        List<Result> resultList = resultService.getResultList( idCompetition );
        for( Result result : resultList ){
            Participant databaseParticipant = getParticipant( result.getResultIdentity().getIdParticipant() );
            if( databaseParticipant != null ){
                if( isMatching( databaseParticipant, participant ) ){
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isMatching( Participant databaseParticipant, Participant participant ){
        return (databaseParticipant.getName().equals( participant.getName() ) &&
            databaseParticipant.getSurename().equals( participant.getSurename() ) &&
            databaseParticipant.getBirthYear() == participant.getBirthYear());
    }

    private Long getID( Participant participant ){
        List<Participant> participantList = getParticipantList();
        for( Participant databaseParticipant : participantList ){
            if( isMatching( databaseParticipant, participant ) ){
                return databaseParticipant.getId();
            }
        }
        return -1L;
    }
}
