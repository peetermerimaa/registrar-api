package valiit.registraator.service;

import valiit.registraator.model.Competition;
import valiit.registraator.repository.CompetitionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CompetitionService{

    @Autowired
    private CompetitionRepository competitionRepository;
    @Autowired
    private ResultService resultService;
    @Autowired
    private ParticipantService participantService;

    public List<Competition> getCompetitionList(){
        return competitionRepository.findAll();
    }

    public Competition getCompetition( Long id ){
        log.info( "fetched competition" );
        return competitionRepository.findById( id ).orElse( null );
    }

    public void addCompetition( Competition competition ){
        if( isNew( competition ) ){
            competitionRepository.save( competition );
        }
    }

    public void deleteCompetition( Long id ){
        if( competitionRepository.existsById( id ) ){
            resultService.deleteResultsByIdCompetition( id );
            competitionRepository.deleteById( id );
        }
    }

    public void editCompetition( Competition competition ){
        Competition newCompetition = getCompetition( competition.getId() );
        newCompetition.setName( competition.getName() );
        newCompetition.setPlace( competition.getPlace() );
        newCompetition.setDate( competition.getDate() );
        newCompetition.setTime( competition.getTime() );
        newCompetition.setDescription( competition.getDescription() );
        competitionRepository.save( newCompetition );
    }

    private boolean isNew( Competition competition ){
        List<Competition> competiitonList = competitionRepository.findAll();
        for( Competition databaseCompetition : competiitonList ){
            if( databaseCompetition.getName().equals( competition.getName() ) &&
                databaseCompetition.getDate().equals( competition.getDate() ) ){
                return false;
            }
        }
        return true;
    }
}
