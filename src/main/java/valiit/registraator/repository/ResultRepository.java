package valiit.registraator.repository;

import valiit.registraator.model.Result;
import valiit.registraator.model.ResultIdentity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ResultRepository extends JpaRepository<Result, ResultIdentity>{
    List<Result> findByResultIdentityIdCompetition( Long idCompetition );

    List<Result> findByResultIdentityIdParticipant( Long idParticipant );
}
